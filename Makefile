CC = gcc
C_FLAGS = -c
PGM = keeplog
REF = listlib

all : $(PGM)

$(PGM) : $(REF).o $(PGM)_helper.o $(PGM).o
	$(CC) $(REF).o $(PGM)_helper.o $(PGM).o

$(REF).o : $(REF).c $(REF).h
	$(CC) $(C_FLAGS) $(REF).c

$(PGM).o : $(PGM).c $(PGM)_helper.h
	$(CC) $(C_FLAGS) $(PGM).c

$(PGM)_helper.o : $(PGM)_helper.c $(REF).h
	$(CC) $(C_FLAGS) $(PGM)_helper.c

clean :
	rm a.out *.o
